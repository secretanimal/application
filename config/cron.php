<?php

return [
    /*
    |--------------------------------------------------------------------------
    | cron
    |--------------------------------------------------------------------------
    |
    | Array que contiene las posibles llamadas al cron
    |
    */

    // Cron alarmas Vinipad Sales Force
    //'01' => '\Rent\Vinipadsalesforcefrontend\Libraries\Cron::checkCallsToQueue',

    // Cron check citas Módulo de Cabinas
    //'02' => '\Rent\Cabinas\Libraries\Cron::checkCitas',

    // Cron para comprobar que hay correos de campañas por enviar a cola de envío o con persistencia activa
    '03' => '\Rent\Comunik\Libraries\Cron::checkCampaignsToCreate',

    // Cron envios de emails
    '04' => '\Rent\Comunik\Libraries\Cron::checkSendEmails',

    // Cron envios de sms
    //'05' => '\Rent\Comunik\Libraries\Cron::checkCallQueueSendSms',

    // Cron vinipad
    //'06' => '\Rent\Vinipadcuadernocata\Libraries\Cron::checkCallsToQueue',

    // Cron comprobación de correos rebotados y número de correos en cada cuenta
    '07' => '\Rent\Comunik\Libraries\Cron::checkBouncedEmailsAccounts',

    // Cron comprobación de correos rebotados y número de correos en cada cuenta
    '08' =>  '\Rent\Forms\Libraries\Cron::checkMessageToSend',
    
    // Cron comprobación de correos rebotados y número de correos en cada cuenta
    '09' =>  '\Rent\Booking\Libraries\Cron::checkVouchersToCreate',

    // Cron to create advanced search exports
    '10' =>  '\Rent\Sergeant\Libraries\Cron::checkAdvancedSearchExports',

    // Cron to delivery schedule reports
    '11' =>  '\Rent\Sergeant\Libraries\Cron::checkReportTaskDelivery',
];