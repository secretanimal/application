<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Catlogue
    |--------------------------------------------------------------------------
    |
    | Routes to public folders
    |
    */

    'libraryFolder'         => '/packages/rent/catalogue/storage/library',
    'tmpFolder'             => '/packages/rent/catalogue/storage/tmp',
    'attachmentFolder'      => '/packages/rent/catalogue/storage/attachment',
    'iconsFolder'           => '/packages/rent/sergeant/images/icons',
];