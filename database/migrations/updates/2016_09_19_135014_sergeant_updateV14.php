<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Rent\Sergeant\Libraries\DBLibrary;

class SergeantUpdateV14 extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		// parameters_022
		DBLibrary::renameColumn('001_022_advanced_search_task', 'parameters_022', 'parameters_022', 'TEXT', null, false, true);
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down(){}
}
