<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SergeantCreateTableCatalogue extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(! Schema::hasTable('001_032_catalogue'))
		{
			Schema::create('001_032_catalogue', function (Blueprint $table) {
				$table->engine = 'InnoDB';
				
				$table->increments('id_032')->unsigned();
				$table->string('name_032',100);
                                $table->string('description_032',150);
				$table->integer('user_id_032')->unsigned();
				
				$table->foreign('user_id_032', 'fk01_001_032_catalogue')
					->references('id_010')
					->on('001_010_user')
					->onDelete('restrict')
					->onUpdate('cascade');

			});
		}
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
