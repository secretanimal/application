<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SergeantCreateTablePlan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(! Schema::hasTable('001_033_plan'))
		{
			Schema::create('001_033_plan', function (Blueprint $table) {
				$table->engine = 'InnoDB';
				
				$table->increments('id_033')->unsigned();
				$table->string('name_033',50);
                                $table->double('price_033',15,4)->unique();
				$table->string('description_033',150);
                                $table->string('alph_code_currency_033',3);
                                
                                $table->foreign('alph_code_currency_033', 'fk01_001_033_plan')
                                    ->references('alphabetic_code_030')
                                    ->on('001_030_currency')
                                    ->onDelete('restrict')
                                    ->onUpdate('cascade');
                                        });
		}
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    
    public function down()
    {
        if (Schema::hasTable('001_033_plan'))
        {
            Schema::drop('001_033_plan');
        }
    }
}
