<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SergeantCreateTablePlaza extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
	{
		if(! Schema::hasTable('001_029_plaza'))
		{
			Schema::create('001_029_plaza', function (Blueprint $table) {
				$table->engine = 'InnoDB';
				
				$table->increments('id_029')->unsigned();
				$table->string('name_029');
				$table->string('address_029');
				$table->integer('user_id_029')->unsigned();
                                
                                $table->foreign('user_id_029', 'fk01_001_029_plaza')
                                    ->references('id_010')
                                    ->on('001_010_user')
                                    ->onDelete('restrict')
                                    ->onUpdate('cascade');
			});
		}
	}

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('001_029_plaza'))
        {
            Schema::drop('001_029_plaza');
        }
    }
}
