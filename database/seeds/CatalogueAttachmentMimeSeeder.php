<?php

use Illuminate\Database\Seeder;
use Rent\Sergeant\Models\AttachmentMime;

class CatalogueAttachmentMimeSeeder extends Seeder
{
    public function run()
    {
        AttachmentMime::insert([
            ['resource_id_019' => 'catalogue-customer', 'mime_019' => 'image/jpeg'],
            ['resource_id_019' => 'catalogue-customer', 'mime_019' => 'image/png'],
            ['resource_id_019' => 'catalogue-customer', 'mime_019' => 'text/plain'],
            ['resource_id_019' => 'catalogue-customer', 'mime_019' => 'application/pdf'],
            ['resource_id_019' => 'catalogue-customer', 'mime_019' => 'application/msword'],
            ['resource_id_019' => 'catalogue-customer', 'mime_019' => 'application/msexcel'],
            ['resource_id_019' => 'catalogue-customer', 'mime_019' => 'application/zip'],
        ]);
    }
}

/*
 * Command to run:
 * php artisan db:seed --class="CatalogoAttachmentMimeSeeder"
 */